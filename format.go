package xxd

// Format configuration of xxd output
type Format struct {
	// Size in bytes per line
	Size int
	// OffsetPad is how many zeros to pad the offset
	OffsetPad int
	// Pack is packing of xxd output.
	// For each multiple of the value, space will be inserted.
	// The default is [2,4,8].
	Pack []int
	// ASCIINone is the character to print in case of unprintable characters for ASCII output, the default is '.'.
	ASCIINone rune
	// ASCII is the text output of the xxd.
	ASCII bool
	// OffsetGap is the gap between the offset and hex output
	OffsetGap int
	// HexGap is the gap between the hex and ASCII output
	HexGap int
}

// DefaultFormat creates the predefined xxd output.
// Size = 16
// OffsetPad = 5
// Pack = [2,4,8]
// ASCIINone = '.'
// ASCII = true
// OffsetGap = 2
// HexGap = 2
func DefaultFormat() Format {
	return Format{16, 5, []int{2, 4, 8}, '.', true, 2, 2}
}
