# Xxd output

Creates xxd output from data coming from a reader

```go
package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"

	"gitlab.com/anickaburova/xxd"
)

func main() {
	format := xxd.DefaultFormat()
	text := "Hello world"
	res, _ := format.XxdString(strings.NewReader(text))
	fmt.Print(res)
	buffer := bufio.NewWriter(os.Stdout)
	format.Xxd(strings.NewReader(text), buffer)
	buffer.Flush()
}
```
