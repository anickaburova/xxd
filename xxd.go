package xxd

import (
	"bytes"
	"fmt"
	"io"
)

// Writer will write string to the output
type Writer interface {
	WriteRune(rune) (int, error)
	WriteString(string) (int, error)
}

// Xxd writes xxd output to the out writer from data reader
func (f *Format) Xxd(data io.Reader, writer Writer) (int, error) {
	size := 0
	buffer := make([]byte, f.Size)
	offsetpad := fmt.Sprintf("%%0%dx:%%%ds", f.OffsetPad, f.OffsetGap)
	hexSize := f.Size*2 + f.HexGap
	for _, p := range f.Pack {
		if p != 0 {
			hexSize += (f.Size - 1) / p
		}
	}

	for offset := 0; ; offset++ {
		l, _ := data.Read(buffer)
		if l > 0 {
			s, err := writer.WriteString(fmt.Sprintf(offsetpad, offset*f.Size, ""))
			if err != nil {
				return size, err
			}
			size += s
			s, err = f.writeHex(buffer[:l], writer)
			if err != nil {
				return size, err
			}
			size += s
			if f.ASCII {
				pad := hexSize - s
				if pad > 0 {
					for i := 0; i < pad; i++ {
						s, err := writer.WriteRune(' ')
						size += s
						if err != nil {
							return size, err
						}
					}
				}
				s, err := f.writeASCII(buffer[:l], writer)
				if err != nil {
					return size, err
				}
				size += s
			}
			s, err = writer.WriteRune('\n')
			if err != nil {
				return size, err
			}
			size += s
		} else {
			break
		}
	}
	return size, nil
}

// XxdString will return xxd string
func (f *Format) XxdString(data io.Reader) (string, error) {
	var result bytes.Buffer
	_, err := f.Xxd(data, &result)
	return result.String(), err
}

func (f *Format) writeHex(data []byte, writer Writer) (int, error) {
	size := 0
	pack := make([]int, len(f.Pack))
	for i, value := range data {
		s, err := writer.WriteString(fmt.Sprintf("%02x", value))
		if err != nil {
			return size, err
		}
		size += s
		if i == len(data)-1 {
			continue
		}
		for i := 0; i < len(f.Pack); i++ {
			pack[i]++
			if pack[i] == f.Pack[i] {
				s, _ := writer.WriteRune(' ')
				if err != nil {
					return size, err
				}
				size += s
				pack[i] = 0
			}
		}
	}
	return size, nil
}

func (f *Format) writeASCII(data []byte, writer Writer) (int, error) {
	size := 0
	for _, value := range data {
		if 32 <= value && value < 127 {
			s, err := writer.WriteRune(rune(value))
			if err != nil {
				return size, err
			}
			size += s
		} else {
			s, err := writer.WriteRune(f.ASCIINone)
			if err != nil {
				return size, err
			}
			size += s
		}
	}
	return size, nil
}
